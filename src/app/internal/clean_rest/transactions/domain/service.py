from typing import Dict, List, Optional, Union

from app.internal.clean_rest.bank_account.db.repositories import IBankAccountRepository
from app.internal.clean_rest.cards.db.repositories import ICardRepository
from app.internal.clean_rest.person.db.models import Person
from app.internal.clean_rest.transactions.db.repositories import ITransactionRepository


class TransactionService:
    def __init__(
        self, transaction_repo: ITransactionRepository, card_repo: ICardRepository, bank_repo: IBankAccountRepository
    ):
        self.bank_repo = bank_repo
        self.card_repo = card_repo
        self.transaction_repo = transaction_repo

    def get_card_history(self, person: Person, card_number: int) -> Optional[List[Dict[str, Union[str, int]]]]:
        card = self.card_repo.get_user_card(person, card_number)
        if card is None:
            return
        return list(self.transaction_repo.get_card_transactions(person.id, card))

    def get_bank_account_history(
        self, person: Person, bank_account_id: int
    ) -> Optional[List[Dict[str, Union[str, int]]]]:
        bank_account = self.bank_repo.get_user_bank_account(person, bank_account_id)
        if bank_account is None:
            return
        return list(self.transaction_repo.get_bank_account_transactions(person.id, bank_account))

    def get_user_transactions(self, person: Person) -> List[Dict[str, Union[str, int]]]:
        return list(self.transaction_repo.get_user_transactions(person.id))

    def get_user_transaction_targets(self, person: Person) -> List[str]:
        return list(self.transaction_repo.get_user_transaction_targets(person))
