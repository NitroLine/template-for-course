from ninja.orm import create_schema

from app.internal.clean_rest.person.db.models import Person

UserSchema = create_schema(Person, exclude=["id", "password", "favorites"])


class UserOut(UserSchema):
    ...
