from decimal import Decimal

import pytest

from app.internal.bot.handlers import balance, card_balance
from app.internal.bot.messages import BalanceMessages, CardBalanceMessages
from app.internal.clean_rest.bank_account.db.models import BankAccount
from app.internal.clean_rest.cards.db.models import Card
from app.internal.clean_rest.person.db.models import Person

TWO_PLACES = Decimal(10) ** -2


@pytest.mark.integration
class TestBalanceHandler:
    @pytest.mark.django_db
    def test_balance(self, test_update, test_context, test_user_with_account_and_card: (Person, BankAccount, Card)):
        user, account, card = test_user_with_account_and_card
        test_update.effective_user.id = user.telegram_id
        balance(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(
            BalanceMessages.show_balance(card.amount.quantize(TWO_PLACES))
        )


@pytest.mark.integration
class TestCardBalanceHandler:
    @pytest.mark.django_db
    def test_card_balance(
        self, test_update, test_context, test_user_with_account_and_card: (Person, BankAccount, Card)
    ):
        user, account, card = test_user_with_account_and_card
        test_update.effective_user.id = user.telegram_id
        test_context.args = [card.number]
        card_balance(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(
            CardBalanceMessages.success(card.amount.quantize(TWO_PLACES))
        )

    @pytest.mark.django_db
    def test_card_balance_no_args(
        self, test_update, test_context, test_user_with_account_and_card: (Person, BankAccount, Card)
    ):
        user, account, card = test_user_with_account_and_card
        test_update.effective_user.id = user.telegram_id
        card_balance(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(CardBalanceMessages.wrong_arguments())
