from ninja import Body

from app.internal.clean_rest.auth.domain.entities import CredentialsSchema, JWTTokensSchema
from app.internal.clean_rest.auth.domain.services import AuthService
from app.internal.clean_rest.errors.errors import NoAccessException, UnauthorizedException


class AuthHandlers:
    def __init__(self, auth_service: AuthService):
        self.auth_service = auth_service

    def login(self, request, credentials: CredentialsSchema = Body(...)) -> JWTTokensSchema:
        tokens, err = self.auth_service.auth_by_password(credentials)
        if err:
            raise UnauthorizedException(err)
        return tokens

    def refresh(self, request, refresh_token: str = Body(...)) -> JWTTokensSchema:
        tokens, err = self.auth_service.validate_and_refresh_tokens(refresh_token)
        if err:
            raise NoAccessException(err)
        return tokens
