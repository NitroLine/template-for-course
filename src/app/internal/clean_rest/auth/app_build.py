from ninja import NinjaAPI

from app.internal.clean_rest.auth.db.repositories import JWTAuthRepository
from app.internal.clean_rest.auth.domain.services import AuthService
from app.internal.clean_rest.auth.presentation.handlers import AuthHandlers
from app.internal.clean_rest.auth.presentation.routers import add_authentication_router
from app.internal.clean_rest.person.db.repositories import PersonRepository


def add_auth_api(api: NinjaAPI):
    user_repo = PersonRepository()
    token_repo = JWTAuthRepository()
    auth_service = AuthService(auth_repo=token_repo, person_repo=user_repo)
    auth_handler = AuthHandlers(auth_service)
    add_authentication_router(api, auth_handler)
