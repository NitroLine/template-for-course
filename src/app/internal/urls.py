from django.urls import path

from app.internal.transport.rest.handlers import PersonView

urlpatterns = [
    path("me/<int:telegram_id>", PersonView.as_view(), name="me_info"),
]
