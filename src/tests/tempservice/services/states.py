PERSON_NOT_IN_FAVORITE = "Person not in favorite"

PERSON_NOT_EXIST = "Person does not exist"

INVALID_PASSWORD = "Invalid password"

NO_SUCH_USER_CARD = "This user does not have such a card"

NO_SUCH_USER_BANK_ACCOUNT = "This user does not have such a bank account"

CARD_NOT_EXIST = "Card does not exist"

NOT_ENOUGH_MONEY = "Not enough money"

TRANSACTION_ERROR = "Error during transaction"

TOKEN_REVOKED = "Refresh token was revoked"
