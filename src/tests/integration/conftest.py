from unittest.mock import MagicMock

import pytest


@pytest.fixture(scope="function")
def test_update():
    effective_user_mock = MagicMock()
    effective_user_mock.first_name = "first_name"
    effective_user_mock.last_name = ""
    effective_user_mock.id = 101
    effective_user_mock.username = "username"
    message_mock = MagicMock()
    message_mock.reply_text = MagicMock(return_value=None)
    update_mock = MagicMock()
    update_mock.message = message_mock
    update_mock.effective_user = effective_user_mock
    return update_mock


@pytest.fixture(scope="function")
def test_context():
    context_mock = MagicMock()
    context_mock.args = []
    return context_mock
