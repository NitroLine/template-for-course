from django.contrib import admin

from app.internal.clean_rest.transactions.db.models import Transaction


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ("date", "card_from", "amount", "card_to")
    pass
