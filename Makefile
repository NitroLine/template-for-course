migrate:
	docker-compose run --rm rest python src/manage.py migrate $(if $m, api $m,)

makemigrations:
	docker-compose run --rm rest python src/manage.py makemigrations

createsuperuser:
	docker-compose run --rm rest python src/manage.py createsuperuser

collectstatic:
	docker-compose run --rm rest python src/manage.py collectstatic --no-input

dev:
	docker-compose up

command:
	docker-compose run --rm rest python src/manage.py ${c}

shell:
	docker-compose run --rm rest python src/manage.py shell

debug:
	docker-compose run --rm rest python src/manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint_docker:
	docker-compose run --rm rest sh -c "isort --check --diff . && flake8 --config setup.cfg && black --check --config pyproject.toml ."

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

dockerstart:
	docker-compose up --build -d

build:
	docker-compose build

up:
	docker-compose up

test:
	docker-compose run --rm rest pytest src/

down:
	docker-compose down