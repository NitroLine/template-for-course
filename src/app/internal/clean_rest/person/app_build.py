from ninja import NinjaAPI

from app.internal.clean_rest.person.db.repositories import PersonRepository
from app.internal.clean_rest.person.domain.services import PersonService
from app.internal.clean_rest.person.presentation.handlers import PersonHandlers
from app.internal.clean_rest.person.presentation.routes import add_users_router


def add_users_api(api: NinjaAPI):
    user_repo = PersonRepository()
    user_service = PersonService(user_repo)
    user_handler = PersonHandlers(user_service)
    add_users_router(api, user_handler)
