from typing import Optional

from django.http import HttpRequest, HttpResponse, JsonResponse

from tests.tempservice.services.auth import TokenTypes, decode_token, is_token_alive


class JWTAuthMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request: HttpRequest) -> HttpResponse:
        if "api/" not in request.path:
            return self.get_response(request)
        token = self._get_bearer_token(request)
        payload = decode_token(token)
        error = None
        if not payload:
            error = {"success": False, "error": "Unauthorized"}
        if payload and not payload.get("type") != TokenTypes.ACCESS:
            error = {"success": False, "error": "Invalid token type"}
        if payload and not is_token_alive(payload, TokenTypes.ACCESS):
            error = {"success": False, "error": "Token expired"}
        if error is not None:
            return JsonResponse(error, status=403)
        return self.get_response(request)

    @staticmethod
    def _get_bearer_token(request: HttpRequest) -> Optional[str]:
        parameters = request.headers.get("Authorization", "").split()
        if len(parameters) != 2 or parameters[0].lower() != "bearer":
            return None
        return parameters[1]
