from django.core.validators import MinValueValidator
from django.db import models

from app.internal.clean_rest.cards.db.models import Card


class Transaction(models.Model):
    card_from = models.ForeignKey(Card, on_delete=models.SET_NULL, null=True, related_name="transaction_from")
    card_to = models.ForeignKey(Card, on_delete=models.SET_NULL, null=True, related_name="transaction_to")
    amount = models.DecimalField(decimal_places=2, max_digits=20, validators=[MinValueValidator(0)])
    date = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return f"Transaction from ${self.date} on amount ${self.amount}"
