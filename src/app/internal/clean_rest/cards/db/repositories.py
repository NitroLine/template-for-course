from decimal import Decimal
from typing import Optional

from django.db import IntegrityError, transaction
from django.db.models import F, QuerySet, Sum

from app.internal.clean_rest.bank_account.db.models import BankAccount
from app.internal.clean_rest.cards.db.models import Card
from app.internal.clean_rest.errors.states import NO_SUCH_USER_CARD, TRANSACTION_ERROR
from app.internal.clean_rest.person.db.models import Person


class ICardRepository:
    def create_card(self, bank_account: BankAccount, amount: Decimal) -> Card:
        ...

    def get_user_card(self, person: Person, card_number: int) -> Optional[Card]:
        ...

    def get_card_by_number(self, card_number: int) -> Optional[Card]:
        ...

    def move_money(self, card_from: Card, card_to: Card, amount: Decimal) -> Optional[str]:
        ...

    def get_card_balance(self, person: Person, card_number: int) -> (Decimal, Optional[str]):
        ...

    def get_bank_account_balance(self, account: BankAccount) -> Decimal:
        ...

    def get_all_user_cards(self, person: Person) -> QuerySet[Card]:
        ...

    def get_user_balance(self, person: Person) -> Decimal:
        ...

    def get_first_user_card(self, person: Person) -> Optional[Card]:
        ...


class CardRepository(ICardRepository):
    def create_card(self, bank_account: BankAccount, amount: Decimal) -> Card:
        return Card.objects.create(bank_account=bank_account, amount=amount)

    def get_user_card(self, person: Person, card_number: int) -> Optional[Card]:
        return Card.objects.filter(bank_account__person=person, number=card_number).first()

    def get_all_user_cards(self, person: Person) -> QuerySet[Card]:
        return Card.objects.filter(bank_account__person=person).all()

    def get_first_user_card(self, person: Person) -> Optional[Card]:
        return Card.objects.filter(bank_account__person=person).first()

    def get_card_by_number(self, card_number: int) -> Optional[Card]:
        return Card.objects.filter(number=card_number).first()

    def move_money(self, card_from: Card, card_to: Card, amount: Decimal) -> Optional[str]:
        card_from.amount = F("amount") - amount
        card_to.amount = F("amount") + amount
        try:
            with transaction.atomic():
                card_to.save(update_fields=("amount",))
                card_from.save(update_fields=("amount",))
        except IntegrityError:
            return TRANSACTION_ERROR

    def get_card_balance(self, person: Person, card_number: int) -> (Decimal, Optional[str]):
        card = Card.objects.filter(bank_account__person=person, number=card_number).values("amount").first()
        if card is None:
            return Decimal(0), NO_SUCH_USER_CARD
        return card["amount"], None

    def get_bank_account_balance(self, account: BankAccount) -> Decimal:
        result = Card.objects.filter(bank_account__id=account.id).aggregate(balance=Sum("amount"))
        return result["balance"] or Decimal(0)

    def get_user_balance(self, person: Person) -> Decimal:
        result = Card.objects.filter(bank_account__person=person).aggregate(balance=Sum("amount"))
        return result["balance"] or Decimal(0)
