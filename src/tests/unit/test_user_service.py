import pytest

from app.internal.clean_rest.person.db.models import Person
from tests.tempservice.services.states import PERSON_NOT_EXIST
from tests.tempservice.services.user_service import (
    add_to_favorite,
    create_person,
    get_person_by_id,
    remove_from_favorite,
    request_person_data,
    set_phone_on_person,
)

NOT_EXIST_ID = 333451


@pytest.mark.unit
class TestUserService:
    @pytest.mark.django_db
    def test_get_person_by_id_when_exist(self, test_user: Person):
        person = get_person_by_id(test_user.telegram_id)
        assert person == test_user

    @pytest.mark.django_db
    def test_save_phone_number(self, test_user: Person):
        new_phone = "+78005553535"
        result = set_phone_on_person(test_user.telegram_id, new_phone)
        assert result
        person = Person.objects.filter(telegram_id=test_user.telegram_id).first()
        assert person
        assert person.phone_number == new_phone

    @pytest.mark.django_db
    def test_create_person(self):
        updated_dict = {"telegram_id": 111, "first_name": "Dan", "username": "dan"}
        person, created = create_person(**updated_dict)
        assert created
        assert person.first_name == updated_dict["first_name"]

    @pytest.mark.django_db
    def test_create_person_when_exist(self, test_user: Person):
        updated_dict = {"telegram_id": test_user.telegram_id, "first_name": "named", "username": test_user.username}
        person, created = create_person(**updated_dict)
        assert not created
        assert person.first_name == test_user.first_name

    @pytest.mark.django_db
    def test_get_person_by_id_when_not_exist(self):
        user = get_person_by_id(telegram_id=0)
        assert user is None

    @pytest.mark.django_db
    def test_add_to_favorite(self, test_user, test_user_without_phone):
        err = add_to_favorite(test_user.telegram_id, test_user_without_phone.username)
        assert err is None
        favorites = list(
            Person.objects.filter(telegram_id=test_user.telegram_id)
            .first()
            .favorites.all()
            .values_list("username", flat=True)
        )
        assert test_user_without_phone.username in favorites

    @pytest.mark.django_db
    def test_add_to_favorite_unknown(self, test_user):
        err = add_to_favorite(test_user.telegram_id, "NOT_EXIST_USER_NAME")
        assert err == PERSON_NOT_EXIST

    @pytest.mark.django_db
    def test_remove_from_favorite(self, test_user_with_favorite_list):
        user1, user2 = test_user_with_favorite_list
        err = remove_from_favorite(user1.telegram_id, user2.username)
        assert err is None
        favorites = list(
            Person.objects.filter(telegram_id=user1.telegram_id)
            .first()
            .favorites.all()
            .values_list("username", flat=True)
        )
        assert user2.username not in favorites

    @pytest.mark.django_db
    def test_request_user_data_without_phone(self, test_user_without_phone):
        info = request_person_data(test_user_without_phone.telegram_id)
        assert info is None

    @pytest.mark.django_db
    def test_request_user_data(self, test_user):
        info = request_person_data(test_user.telegram_id)
        assert info is not None
