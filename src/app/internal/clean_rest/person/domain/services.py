from typing import Optional

from app.internal.clean_rest.auth.domain.services import AuthService
from app.internal.clean_rest.errors.states import PERSON_NOT_EXIST
from app.internal.clean_rest.person.db.models import Person
from app.internal.clean_rest.person.db.repositories import IPersonRepository
from app.internal.clean_rest.person.domain.entities import UserOut


class PersonService:
    def __init__(self, user_repo: IPersonRepository):
        self._person_repo = user_repo

    def request_person_data(self, person: Person) -> Optional[UserOut]:
        if person.phone_number:
            return UserOut.from_orm(person)

    def create_person(self, telegram_id: int, first_name: str, username: str) -> (UserOut, bool):
        user, created = self._person_repo.create_person(telegram_id, first_name, username)
        return UserOut.from_orm(user), created

    def get_person_by_id(self, telegram_id: int) -> Optional[Person]:
        return self._person_repo.get_person_by_id(telegram_id)

    def get_user_by_id(self, telegram_id: int) -> Optional[UserOut]:
        person = self.get_person_by_id(telegram_id)
        if person is None:
            return None
        return UserOut.from_orm(person)

    def get_user_by_username(self, username: str) -> Optional[UserOut]:
        person = self._person_repo.get_person_by_username(username)
        if person is None:
            return None
        return UserOut.from_orm(person)

    def get_person_by_username(self, username: str) -> Optional[UserOut]:
        return self._person_repo.get_person_by_username(username)

    def get_person_id(self, telegram_id: int) -> Optional[str]:
        return self._person_repo.get_person_id(telegram_id)

    def set_phone_on_person(self, person: Person, phone: str) -> None:
        self._person_repo.set_phone_on_person(person, phone)

    def set_user_password(self, person: Person, password: str) -> bool:
        if person.phone_number:
            self._person_repo.set_user_password(person, AuthService.hash_user_password(password))
            return True
        return False

    def add_to_favorite(self, person: Person, favorite_username: str) -> Optional[str]:
        favorite_person = self._person_repo.get_person_by_username(favorite_username)
        if not favorite_person:
            return PERSON_NOT_EXIST
        return self._person_repo.add_to_favorite(person, favorite_person)

    def remove_from_favorite(self, person: Person, favorite_username: str) -> Optional[str]:
        favorite_person = self._person_repo.get_person_by_username(favorite_username)
        if not favorite_person:
            return PERSON_NOT_EXIST
        return self._person_repo.remove_from_favorite(person, favorite_person)

    def get_user_favorites(self, person: Person) -> [str]:
        return self._person_repo.get_user_favorites(person)
