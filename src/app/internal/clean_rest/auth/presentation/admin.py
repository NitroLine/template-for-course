from django.contrib import admin

from app.internal.clean_rest.auth.db.models import IssuedToken


@admin.register(IssuedToken)
class TokenAdmin(admin.ModelAdmin):
    pass
