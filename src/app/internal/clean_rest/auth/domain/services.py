import hashlib
import hmac
from typing import Optional

from app.internal.clean_rest.auth.db.repositories import IAuthRepository, TokenTypes
from app.internal.clean_rest.auth.domain.entities import CredentialsSchema, JWTTokensSchema
from app.internal.clean_rest.errors.states import INVALID_PASSWORD, PERSON_NOT_EXIST
from app.internal.clean_rest.person.db.repositories import IPersonRepository
from config.settings import PASSWORD_SALT, TOKENS_TTL


class AuthService:
    def __init__(self, auth_repo: IAuthRepository, person_repo: IPersonRepository):
        self._auth_repo = auth_repo
        self._person_repo = person_repo

    def validate_and_refresh_tokens(self, refresh_token: str) -> (Optional[JWTTokensSchema], Optional[str]):
        issued_token = self._auth_repo.get_issued_token(refresh_token)
        if issued_token is None:
            return None, "Unknown refresh token"
        payload = self._auth_repo.decode_token(refresh_token)
        if payload is None:
            return None, "Invalid token"
        if payload.get("type", None) != TokenTypes.REFRESH:
            return None, "Invalid token type"
        if not self._auth_repo.is_token_alive(payload, TokenTypes.REFRESH):
            return None, "Token expired"
        tokens, err = self._auth_repo.refresh_tokens(issued_token)
        if err:
            return None, err
        return JWTTokensSchema(access_token=tokens[0], refresh_token=tokens[1]), None

    def auth_by_telegram_id(self, telegram_id: int) -> (Optional[JWTTokensSchema], Optional[str]):
        person = self._person_repo.get_person_by_id(telegram_id)
        if person is None:
            return None, PERSON_NOT_EXIST
        tokens = self._auth_repo.generate_new_tokens(person)
        return JWTTokensSchema(access_token=tokens[0], refresh_token=tokens[1]), None

    def auth_by_password(self, credentials: CredentialsSchema) -> (Optional[JWTTokensSchema], Optional[str]):
        person = self._person_repo.get_person_by_username(credentials.username)
        if person is None:
            return None, PERSON_NOT_EXIST
        hashed_pass = self.hash_user_password(credentials.password)
        if person.password.tobytes() != hashed_pass:
            return None, INVALID_PASSWORD
        tokens = self._auth_repo.generate_new_tokens(person)
        return JWTTokensSchema(access_token=tokens[0], refresh_token=tokens[1]), None

    @staticmethod
    def hash_user_password(password: str) -> bytes:
        return hmac.new(PASSWORD_SALT.encode(), password.encode(), hashlib.sha512).digest()
