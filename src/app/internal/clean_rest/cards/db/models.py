from django.core.validators import MinValueValidator
from django.db import models

from app.internal.clean_rest.bank_account.db.models import BankAccount


class Card(models.Model):
    bank_account = models.ForeignKey(BankAccount, on_delete=models.CASCADE)
    amount = models.DecimalField(decimal_places=2, max_digits=20, validators=[MinValueValidator(0)])
    number = models.AutoField(auto_created=True, primary_key=True)

    def __str__(self):
        return f"Card ${self.number}"
