from ninja import NinjaAPI

from app.internal.clean_rest.auth.app_build import add_auth_api
from app.internal.clean_rest.auth.jwt_bearer import HTTPJWTAuth
from app.internal.clean_rest.bank_account.app_build import add_bank_accounts_api
from app.internal.clean_rest.cards.app_build import add_cards_api
from app.internal.clean_rest.errors.app_build import configure_error_api
from app.internal.clean_rest.person.app_build import add_users_api


def get_api():
    api = NinjaAPI(title="backend course", version="1.0.0", auth=[HTTPJWTAuth()])
    add_users_api(api)
    add_auth_api(api)
    add_cards_api(api)
    add_bank_accounts_api(api)
    configure_error_api(api)
    return api
