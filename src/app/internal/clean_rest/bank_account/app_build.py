from ninja import NinjaAPI

from app.internal.clean_rest.bank_account.db.repositories import BankAccountRepository
from app.internal.clean_rest.bank_account.presentations.handlers import BankAccountHandlers
from app.internal.clean_rest.bank_account.presentations.routes import add_bank_account_router


def add_bank_accounts_api(api: NinjaAPI):
    bank_account_repo = BankAccountRepository()
    account_handler = BankAccountHandlers(bank_account_repo)
    add_bank_account_router(api, account_handler)
