import hashlib
import hmac
from typing import Optional

from app.internal.clean_rest.bank_account.db.models import BankAccount
from app.internal.clean_rest.cards.db.models import Card
from app.internal.clean_rest.person.db.models import Person
from config.settings import PASSWORD_SALT
from tests.tempservice.services.states import PERSON_NOT_EXIST


def create_person(telegram_id: int, first_name: str, username: str) -> (Person, bool):
    return Person.objects.get_or_create(
        telegram_id=telegram_id,
        defaults={
            "username": username,
            "first_name": first_name,
        },
    )


def get_person_by_id(telegram_id: int) -> Optional[Person]:
    return Person.objects.filter(telegram_id=telegram_id).first()


def get_person_by_username(username: str) -> Optional[Person]:
    return Person.objects.filter(username=username).first()


def get_person_id(telegram_id: int) -> Optional[str]:
    result = Person.objects.filter(telegram_id=telegram_id).values("id").first()
    if result is not None:
        return result["id"]


def set_phone_on_person(telegram_id: int, phone: str) -> bool:
    person = get_person_by_id(telegram_id)
    if person is None:
        return False
    person.phone_number = phone
    person.save()
    return True


def hash_user_password(password: str) -> bytes:
    return hmac.new(PASSWORD_SALT.encode(), password.encode(), hashlib.sha512).digest()


def set_user_password(telegram_id: int, password: str) -> bool:
    person = get_person_by_id(telegram_id)
    if person is None or not person.phone_number:
        return False
    person.password = hash_user_password(password)
    person.save()
    return True


def request_person_data(telegram_id: int) -> Optional[dict]:
    person = get_person_by_id(telegram_id)
    if person and person.phone_number:
        return {
            "phone_number": person.phone_number,
            "username": person.username,
            "first_name": person.first_name,
            "telegram_id": person.telegram_id,
            "сards_numbers": list(Card.objects.filter(bank_account__person=person.id).values_list("number", flat=True)),
            "favorites": list(person.favorites.all().values_list("username", flat=True)),
            "banks_accounts": list(BankAccount.objects.filter(person=person.id).values_list("id", flat=True)),
        }


def add_to_favorite(telegram_id: int, favorite_username: str) -> Optional[str]:
    person = get_person_by_id(telegram_id)
    if not person:
        return PERSON_NOT_EXIST
    favorite_person = Person.objects.filter(username=favorite_username).first()
    if not favorite_person:
        return PERSON_NOT_EXIST
    person.favorites.add(favorite_person)
    person.save()


def remove_from_favorite(telegram_id: int, favorite_username: str) -> Optional[str]:
    person = get_person_by_id(telegram_id)
    if not person:
        return PERSON_NOT_EXIST
    favorite_person = Person.objects.filter(username=favorite_username).first()
    if not favorite_person:
        return PERSON_NOT_EXIST
    person.favorites.remove(favorite_person)
    person.save()
