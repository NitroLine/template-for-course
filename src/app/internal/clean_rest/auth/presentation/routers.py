from ninja import NinjaAPI, Router

from app.internal.clean_rest.auth.domain.entities import JWTTokensSchema
from app.internal.clean_rest.auth.presentation.handlers import AuthHandlers
from app.internal.clean_rest.default.responce import ErrorResponse


def add_authentication_router(api: NinjaAPI, issued_token_handlers: AuthHandlers):
    issued_token_router = get_authentication_router(issued_token_handlers)
    api.add_router("/authentication", issued_token_router)


def get_authentication_router(authentication_handlers: AuthHandlers):
    router = Router(tags=["authentication"])

    router.add_api_operation(
        "/login",
        ["POST"],
        authentication_handlers.login,
        auth=None,
        response={200: JWTTokensSchema, 400: ErrorResponse},
    )

    router.add_api_operation(
        "/refresh",
        ["POST"],
        authentication_handlers.refresh,
        auth=None,
        response={200: JWTTokensSchema, 400: ErrorResponse},
    )

    return router
