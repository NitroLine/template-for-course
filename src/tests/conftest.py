from decimal import Decimal

import pytest

from app.internal.clean_rest.bank_account.db.models import BankAccount
from app.internal.clean_rest.cards.db.models import Card
from app.internal.clean_rest.person.db.models import Person
from app.internal.clean_rest.transactions.db.models import Transaction


@pytest.fixture(scope="function")
def test_user(telegram_id=101, first_name="first_name", username="username"):
    return Person.objects.create(
        telegram_id=telegram_id, first_name=first_name, username=username, phone_number="8" * 11
    )


@pytest.fixture(scope="function")
def test_users_with_transactions(first_telegram_id=1, first_name="User1", second_telegram_id=2, second_name="User2"):
    user_sender = Person.objects.create(telegram_id=first_telegram_id, first_name=first_name, username=first_name)
    user_receiving = Person.objects.create(telegram_id=second_telegram_id, first_name=second_name, username=second_name)
    bank_account_sender = BankAccount.objects.create(id=22, person=user_sender)
    bank_account_receiving = BankAccount.objects.create(id=23, person=user_receiving)
    sender_card = Card.objects.create(amount=Decimal(1000), bank_account=bank_account_sender, number=49)
    receiving_card = Card.objects.create(amount=Decimal(2000), bank_account=bank_account_receiving, number=51)
    transaction = Transaction.objects.create(card_from=sender_card, card_to=receiving_card, amount=Decimal(50))
    return (
        (user_sender, bank_account_sender, sender_card),
        (user_receiving, bank_account_receiving, receiving_card),
        transaction,
    )


@pytest.fixture(scope="function")
def test_user_without_phone(telegram_id=120, first_name="set_name", username="no_number"):
    return Person.objects.create(
        telegram_id=telegram_id,
        first_name=first_name,
        username=username,
    )


@pytest.fixture(scope="function")
def test_user_with_account_and_card():
    user = Person.objects.create(
        telegram_id=10, first_name="TestUserWithBank1", username="bank_user1", phone_number="8" + "0" * 10
    )
    bank_account = BankAccount.objects.create(id=25, person=user)
    card = Card.objects.create(amount=Decimal(5000), bank_account=bank_account, number=50)
    return user, bank_account, card


@pytest.fixture(scope="function")
def test_user_with_account_and_two_cards():
    user = Person.objects.create(telegram_id=100, first_name="TestUserWithBank2", phone_number="8" + "3" * 10)
    bank_account = BankAccount.objects.create(id=22, person=user)
    first_card = Card.objects.create(amount=Decimal(1000), bank_account=bank_account, number=49)
    second_card = Card.objects.create(amount=Decimal(2000), bank_account=bank_account, number=51)
    return user, bank_account, first_card, second_card


@pytest.fixture(scope="function")
def test_user_with_favorite_list(first_telegram_id=1, first_name="User1", second_telegram_id=2, second_name="User2"):
    user_with_favorite = Person.objects.create(
        telegram_id=first_telegram_id, first_name=first_name, username=first_name
    )
    user_in_somebody_favorite = Person.objects.create(
        telegram_id=second_telegram_id, first_name=second_name, username=second_name
    )
    user_with_favorite.favorites.add(user_in_somebody_favorite)
    return user_with_favorite, user_in_somebody_favorite
