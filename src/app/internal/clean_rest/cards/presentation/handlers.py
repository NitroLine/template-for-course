from ninja import Body

from app.internal.clean_rest.cards.domain.entities import CardIn, CardOut, TransactionIn
from app.internal.clean_rest.cards.domain.service import CardService
from app.internal.clean_rest.default.responce import SuccessResponse
from app.internal.clean_rest.errors.errors import BadRequestException


class CardHandlers:
    def __init__(self, card_service: CardService):
        self.card_service = card_service

    def create_user_card(self, request, card_data: CardIn = Body(...)) -> CardOut:
        card_out, err = self.card_service.create_user_card(request.user, card_data)
        if err:
            raise BadRequestException(err)
        return card_out

    def get_card_balance(self, request, card_number: int = Body(...)) -> int:
        balance, err = self.card_service.get_card_balance(request.user, card_number)
        if err:
            raise BadRequestException(err)
        return balance

    def move_money(self, request, transaction_data: TransactionIn = Body(...)) -> SuccessResponse:
        if transaction_data.amount <= 0:
            raise BadRequestException("Not valid amount")
        err = self.card_service.move_money(request.user, transaction_data)
        if err is not None:
            raise BadRequestException(err)
        return SuccessResponse(success=True)

    def get_user_cards(self, request) -> [CardOut]:
        return self.card_service.get_all_user_cards(request.user)
