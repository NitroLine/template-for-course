from typing import List

from ninja import NinjaAPI, Router

from app.internal.clean_rest.cards.domain.entities import CardOut
from app.internal.clean_rest.cards.presentation.handlers import CardHandlers
from app.internal.clean_rest.default.responce import ErrorResponse, SuccessResponse


def add_card_router(api: NinjaAPI, card_handlers: CardHandlers):
    users_router = get_card_router(card_handlers)
    api.add_router("/cards", users_router)


def get_card_router(card_handlers: CardHandlers):
    router = Router(tags=["cards"])

    router.add_api_operation(
        "/all",
        ["GET"],
        card_handlers.get_user_cards,
        response={200: List[CardOut], 400: ErrorResponse},
    )

    router.add_api_operation(
        "/balance",
        ["POST"],
        card_handlers.get_card_balance,
        response={200: int, 400: ErrorResponse},
    )

    router.add_api_operation(
        "/",
        ["PUT"],
        card_handlers.create_user_card,
        response={200: CardOut, 400: ErrorResponse},
    )

    router.add_api_operation(
        "/move",
        ["POST"],
        card_handlers.move_money,
        response={200: SuccessResponse, 400: ErrorResponse},
    )

    return router
