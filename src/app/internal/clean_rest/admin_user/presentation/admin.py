from django.contrib import admin

from app.internal.clean_rest.admin_user.db.models import AdminUser


@admin.register(AdminUser)
class AdminUserAdmin(admin.ModelAdmin):
    pass
