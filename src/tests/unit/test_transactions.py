import pytest

from tests.tempservice.services.bank_service import get_user_transactions

NOT_EXIST_ID = 333451


@pytest.mark.unit
class TestUserService:
    @pytest.mark.django_db
    def test_get_user_transactions_sender(self, test_users_with_transactions: tuple):
        sender_data, receiver_data, transaction = test_users_with_transactions
        user, bank_account, card = sender_data
        transactions, error = get_user_transactions(user.telegram_id)
        assert error is None
        assert len(transactions) == 1
        first_transaction = transactions[0]
        assert first_transaction["card_number_from"] == card.number
        assert first_transaction["username_from"] == user.username
        assert first_transaction["amount"] == transaction.amount
        assert first_transaction["date"] == transaction.date
        user, bank_account, card = receiver_data
        assert first_transaction["card_number_to"] == card.number
        assert first_transaction["user_to"] == user.username
