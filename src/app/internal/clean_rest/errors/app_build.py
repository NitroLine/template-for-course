from functools import partial

from ninja import NinjaAPI

from app.internal.clean_rest.errors.error_handlers import (
    authorization_exception_handler,
    bad_request_handler,
    no_access_exception_handler,
    not_exist_handler,
    phone_number_not_set_handler,
)
from app.internal.clean_rest.errors.errors import (
    BadRequestException,
    NoAccessException,
    NoPhoneNumberException,
    NotExistException,
    UnauthorizedException,
)


def configure_error_api(api: NinjaAPI):
    api.add_exception_handler(NoPhoneNumberException, partial(phone_number_not_set_handler, api=api))
    api.add_exception_handler(NotExistException, partial(not_exist_handler, api=api))
    api.add_exception_handler(BadRequestException, partial(bad_request_handler, api=api))
    api.add_exception_handler(UnauthorizedException, partial(authorization_exception_handler, api=api))
    api.add_exception_handler(NoAccessException, partial(no_access_exception_handler, api=api))
    return api
