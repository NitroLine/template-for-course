from typing import List, Optional

from django.db import IntegrityError, transaction
from django.db.models import F, Q, Sum

from app.internal.clean_rest.bank_account.db.models import BankAccount
from app.internal.clean_rest.cards.db.models import Card
from app.internal.clean_rest.person.db.models import Person
from app.internal.clean_rest.transactions.db.models import Transaction
from tests.tempservice.services.states import (
    CARD_NOT_EXIST,
    NO_SUCH_USER_BANK_ACCOUNT,
    NO_SUCH_USER_CARD,
    NOT_ENOUGH_MONEY,
    PERSON_NOT_EXIST,
    TRANSACTION_ERROR,
)
from tests.tempservice.services.user_service import get_person_by_id, get_person_id


def get_user_balance(telegram_id: int) -> (int, Optional[str]):
    person_id = get_person_id(telegram_id)
    if person_id is None:
        return 0, PERSON_NOT_EXIST
    result = Card.objects.filter(bank_account__person=person_id).aggregate(balance=Sum("amount"))
    return result["balance"] or 0, None


def get_bank_account_balance(account_id: int) -> int:
    result = Card.objects.filter(bank_account__id=account_id).aggregate(balance=Sum("amount"))
    return result["balance"] or 0


def get_card_balance(telegram_id, card_number) -> (int, Optional[str]):
    person_id = get_person_id(telegram_id)
    if person_id is None:
        return 0, PERSON_NOT_EXIST
    card = Card.objects.filter(bank_account__person=person_id, number=card_number).values("amount").first()

    if card is None:
        return 0, NO_SUCH_USER_CARD
    return card["amount"], None


def move_money(telegram_id, card_from_id, card_to_id, amount) -> Optional[str]:
    person_id = get_person_id(telegram_id)
    if person_id is None:
        return PERSON_NOT_EXIST
    card_from = Card.objects.filter(bank_account__person=person_id, number=card_from_id).first()
    if card_from is None:
        return NO_SUCH_USER_CARD
    if card_from.amount - amount < 0:
        return NOT_ENOUGH_MONEY
    card_to = Card.objects.filter(number=card_to_id).first()
    if card_to is None:
        return CARD_NOT_EXIST
    card_from.amount = F("amount") - amount
    card_to.amount = F("amount") + amount
    try:
        with transaction.atomic():
            card_to.save(update_fields=("amount",))
            card_from.save(update_fields=("amount",))
            Transaction.objects.create(card_from=card_from, card_to=card_to, amount=amount)
    except IntegrityError:
        return TRANSACTION_ERROR


def move_money_on_telegram_username(telegram_id, card_from_id, telegram_username, amount) -> Optional[str]:
    person = Person.objects.filter(username=telegram_username).values("id").first()
    if person is None:
        return PERSON_NOT_EXIST
    card_to = Card.objects.filter(bank_account__person=person["id"]).first()
    if card_to is None:
        return CARD_NOT_EXIST
    return move_money(telegram_id, card_from_id, card_to.number, amount)


def get_user_transactions(telegram_id) -> (List[dict], Optional[str]):
    person_id = get_person_id(telegram_id)
    if person_id is None:
        return [], PERSON_NOT_EXIST
    transactions = (
        Transaction.objects.filter(
            Q(card_from__bank_account__person_id=person_id) | Q(card_to__bank_account__person_id=person_id)
        )
        .annotate(
            card_number_from=F("card_from__number"),
            card_number_to=F("card_to__number"),
            username_from=F("card_from__bank_account__person__username"),
            user_to=F("card_to__bank_account__person__username"),
        )
        .values(
            "date",
            "card_number_from",
            "card_number_to",
            "amount",
            "username_from",
            "user_to",
        )
        .all()
    )
    return list(transactions), None


def get_card_transactions(telegram_id, card_number) -> ([dict], Optional[str]):
    person_id = get_person_id(telegram_id)
    if person_id is None:
        return [], PERSON_NOT_EXIST
    if not Card.objects.filter(bank_account__person=person_id, number=card_number).exists():
        return [], NO_SUCH_USER_CARD
    transactions = (
        Transaction.objects.filter(
            Q(card_from__bank_account__person_id=person_id, card_from__number=card_number)
            | Q(card_to__bank_account__person_id=person_id, card_to__number=card_number)
        )
        .annotate(
            card_number_from=F("card_from__number"),
            card_number_to=F("card_to__number"),
            username_from=F("card_from__bank_account__person__username"),
            user_to=F("card_to__bank_account__person__username"),
        )
        .values(
            "date",
            "card_number_from",
            "card_number_to",
            "amount",
            "username_from",
            "user_to",
        )
        .all()
    )
    return list(transactions), None


def get_bank_account_transactions(telegram_id, bank_account_id) -> ([dict], Optional[str]):
    person_id = get_person_id(telegram_id)
    if person_id is None:
        return [], PERSON_NOT_EXIST
    if not BankAccount.objects.filter(person__id=person_id, id=bank_account_id).exists():
        return [], NO_SUCH_USER_BANK_ACCOUNT
    transactions = (
        Transaction.objects.filter(
            Q(card_from__bank_account__person_id=person_id, card_from__bank_account__id=bank_account_id)
            | Q(card_to__bank_account__person_id=person_id, card_to__bank_account__id=bank_account_id)
        )
        .annotate(
            card_number_from=F("card_from__number"),
            card_number_to=F("card_to__number"),
            username_from=F("card_from__bank_account__person__username"),
            user_to=F("card_to__bank_account__person__username"),
        )
        .values(
            "date",
            "card_number_from",
            "card_number_to",
            "amount",
            "username_from",
            "user_to",
        )
        .all()
    )
    return list(transactions), None


def get_user_transaction_targets(telegram_id) -> ([str], Optional[str]):
    person = get_person_by_id(telegram_id)
    if person is None:
        return [], PERSON_NOT_EXIST
    usernames_to = Transaction.objects.filter(card_to__bank_account__person_id=person.id).values_list(
        "card_from__bank_account__person__username", flat=True
    )
    usernames_from = Transaction.objects.filter(card_from__bank_account__person_id=person.id).values_list(
        "card_to__bank_account__person__username", flat=True
    )
    targets = usernames_from.union(usernames_to).all()
    return list(targets), None
