from ninja.orm import create_schema

from app.internal.clean_rest.bank_account.db.models import BankAccount

BankAccountSchema = create_schema(BankAccount)


class BankAccountOut(BankAccountSchema):
    ...
