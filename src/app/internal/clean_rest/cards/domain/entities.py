from decimal import Decimal

from ninja import Schema
from ninja.orm import create_schema

from app.internal.clean_rest.cards.db.models import Card

CardSchema = create_schema(Card)


class CardOut(CardSchema):
    ...


class CardIn(Schema):
    amount: Decimal
    bank_account_id: int


class TransactionIn(Schema):
    card_from_number: int
    card_to_number: int
    amount: Decimal
