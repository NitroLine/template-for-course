from django.core.management.base import BaseCommand

from app.internal.bot.bot import create_telegram_bot


class Command(BaseCommand):
    help = "Run telegram bot"

    def handle(self, *args, **options):
        bot = create_telegram_bot()
        print("Bot is now working")
        bot.start_polling()
        bot.idle()
