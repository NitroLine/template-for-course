from telegram.ext import CommandHandler, Updater

from app.internal.bot.handlers import (
    balance,
    card_balance,
    history,
    history_account,
    history_card,
    login,
    me,
    move_money_to_card,
    move_money_to_user,
    my_senders,
    new_favorite,
    remove_favorite,
    set_password,
    set_phone,
    start,
)
from config.settings import TELEGRAM_TOKEN


def create_telegram_bot():
    bot = Updater(TELEGRAM_TOKEN)
    bot.dispatcher.add_handler(CommandHandler("start", start))
    bot.dispatcher.add_handler(CommandHandler("set_phone", set_phone))
    bot.dispatcher.add_handler(CommandHandler("me", me))
    bot.dispatcher.add_handler(CommandHandler("balance", balance))
    bot.dispatcher.add_handler(CommandHandler("new_favorite", new_favorite))
    bot.dispatcher.add_handler(CommandHandler("remove_favorite", remove_favorite))
    bot.dispatcher.add_handler(CommandHandler("card_balance", card_balance))
    bot.dispatcher.add_handler(CommandHandler("move_money_to_card", move_money_to_card))
    bot.dispatcher.add_handler(CommandHandler("move_money_to_user", move_money_to_user))
    bot.dispatcher.add_handler(CommandHandler("history", history))
    bot.dispatcher.add_handler(CommandHandler("my_senders", my_senders))
    bot.dispatcher.add_handler(CommandHandler("history_card", history_card))
    bot.dispatcher.add_handler(CommandHandler("history_account", history_account))
    bot.dispatcher.add_handler(CommandHandler("login", login))
    bot.dispatcher.add_handler(CommandHandler("set_password", set_password))
    return bot
