from django.contrib import admin

from app.internal.clean_rest.admin_user.presentation.admin import AdminUserAdmin
from app.internal.clean_rest.auth.presentation.admin import TokenAdmin
from app.internal.clean_rest.bank_account.presentations.admin import BankAccountAdmin
from app.internal.clean_rest.cards.presentation.admin import CardAdmin
from app.internal.clean_rest.person.presentation.admin import PersonAdmin
from app.internal.clean_rest.transactions.presentation.admin import TransactionAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
