import pytest

from app.internal.bot.handlers import me


@pytest.mark.integration
class TestMeHandler:
    @pytest.mark.django_db
    def test_me(self, test_update, test_context, test_user):
        me(test_update, test_context)
        test_update.message.reply_text.assert_called_once()
