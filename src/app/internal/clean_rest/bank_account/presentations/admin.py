from django.contrib import admin

from app.internal.clean_rest.bank_account.db.models import BankAccount


@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    list_display = ("id", "person")
    pass
