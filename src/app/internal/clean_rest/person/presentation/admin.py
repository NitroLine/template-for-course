from django.contrib import admin

from app.internal.clean_rest.person.db.models import Person


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ("telegram_id", "first_name", "username", "phone_number")
