from typing import Optional

from django.db.models import QuerySet

from app.internal.clean_rest.bank_account.db.models import BankAccount
from app.internal.clean_rest.person.db.models import Person


class IBankAccountRepository:
    def create_bank_account(self, person: Person) -> BankAccount:
        ...

    def get_user_bank_account(self, person: Person, bank_account_id: int) -> Optional[BankAccount]:
        ...

    def get_bank_account_by_id(self, bank_account_id: int) -> Optional[BankAccount]:
        ...

    def get_user_bank_accounts(self, person: Person) -> QuerySet[BankAccount]:
        ...


class BankAccountRepository(IBankAccountRepository):
    def create_bank_account(self, person: Person) -> BankAccount:
        return BankAccount.objects.create(person=person)

    def get_user_bank_account(self, person: Person, bank_account_id: int) -> Optional[BankAccount]:
        return BankAccount.objects.filter(person=person, id=bank_account_id).first()

    def get_bank_account_by_id(self, bank_account_id: int) -> Optional[BankAccount]:
        return BankAccount.objects.filter(id=bank_account_id).first()

    def get_user_bank_accounts(self, person: Person) -> QuerySet[BankAccount]:
        return BankAccount.objects.filter(person=person)
