import pytest
from django.contrib.auth import get_user_model
from django.test import Client


@pytest.mark.smoke
@pytest.mark.django_db
def test_db():
    user_model = get_user_model()
    user_model.objects.create_user("test", "test@test.com", "test-sec-password")
    assert user_model.objects.count() == 1


@pytest.mark.smoke
def test_web():
    resp = Client().get("/admin/")
    assert resp.status_code == 302
