import re
from decimal import Decimal

from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bot.messages import (
    BalanceMessages,
    CardBalanceMessages,
    HistoryAccountMessages,
    HistoryCardMessages,
    HistoryMessages,
    LoginMessages,
    MeMessages,
    MoveMoneyToCardMessages,
    MoveMoneyToUserMessages,
    NewFavoriteMessages,
    RemoveFavoriteMessages,
    SetPasswordMessages,
    SetPhoneMessages,
    StartMessages,
    TargetsListMessages,
)
from app.internal.clean_rest.auth.db.repositories import JWTAuthRepository
from app.internal.clean_rest.auth.domain.services import AuthService
from app.internal.clean_rest.bank_account.db.repositories import BankAccountRepository
from app.internal.clean_rest.cards.db.repositories import CardRepository
from app.internal.clean_rest.cards.domain.entities import TransactionIn
from app.internal.clean_rest.cards.domain.service import CardService
from app.internal.clean_rest.person.db.repositories import PersonRepository
from app.internal.clean_rest.person.domain.services import PersonService
from app.internal.clean_rest.transactions.db.repositories import TransactionRepository
from app.internal.clean_rest.transactions.domain.service import TransactionService
from config.settings import PHONE_REGEX
from tests.tempservice.services.utils import is_int

person_repo = PersonRepository()
person_service = PersonService(person_repo)
card_repo = CardRepository()
bank_account_repo = BankAccountRepository()
transaction_repo = TransactionRepository()
card_service = CardService(card_repo, transaction_repo, bank_account_repo)
transaction_service = TransactionService(transaction_repo, card_repo, bank_account_repo)
auth_repo = JWTAuthRepository()
auth_service = AuthService(auth_repo, person_repo)


def start(update: Update, context: CallbackContext) -> None:
    person_service.create_person(
        first_name=update.effective_user.first_name,
        telegram_id=update.effective_user.id,
        username=update.effective_user.username,
    )
    update.message.reply_text(StartMessages.hello(update.effective_user.first_name))


def set_phone(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text(SetPhoneMessages.wrong_arguments())
        return
    if not re.match(PHONE_REGEX, context.args[0]):
        update.message.reply_text(SetPhoneMessages.wrong_format())
        return
    person = person_service.get_person_by_id(telegram_id=update.effective_user.id)
    if person is None:
        update.message.reply_text(SetPhoneMessages.user_not_found())
        return
    person_service.set_phone_on_person(person=person, phone=context.args[0])
    update.message.reply_text(SetPhoneMessages.success())


def me(update: Update, context: CallbackContext) -> None:
    person = person_service.get_person_by_id(telegram_id=update.effective_user.id)
    if person is None:
        update.message.reply_text(MeMessages.user_not_found())
        return
    person_data = person_service.request_person_data(person).dict()
    if person:
        update.message.reply_text(MeMessages.me_info(person_data))
    else:
        update.message.reply_text(MeMessages.no_access())


def balance(update: Update, context: CallbackContext) -> None:
    person = person_service.get_person_by_id(update.effective_user.id)
    if person is None:
        update.message.reply_text(BalanceMessages.user_not_found())
        return
    amount = card_repo.get_user_balance(person)
    update.message.reply_text(BalanceMessages.show_balance(amount))


def card_balance(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text(CardBalanceMessages.wrong_arguments())
        return
    person = person_service.get_person_by_id(update.effective_user.id)
    if person is None:
        update.message.reply_text(CardBalanceMessages.user_not_found())
        return
    amount, err = card_repo.get_card_balance(person=person, card_number=context.args[0])
    if err:
        update.message.reply_text(CardBalanceMessages.error(err))
        return
    update.message.reply_text(CardBalanceMessages.success(amount))


def new_favorite(update: Update, context: CallbackContext):
    if len(context.args) != 1:
        update.message.reply_text(NewFavoriteMessages.wrong_arguments())
        return
    person = person_service.get_person_by_id(update.effective_user.id)
    if person is None:
        update.message.reply_text(NewFavoriteMessages.user_not_found())
        return
    err = person_service.add_to_favorite(person=person, favorite_username=context.args[0])
    if err:
        update.message.reply_text(NewFavoriteMessages.error(err))
        return
    update.message.reply_text(NewFavoriteMessages.success())


def remove_favorite(update: Update, context: CallbackContext):
    if len(context.args) != 1:
        update.message.reply_text(RemoveFavoriteMessages.wrong_arguments())
        return
    person = person_service.get_person_by_id(update.effective_user.id)
    if person is None:
        update.message.reply_text(RemoveFavoriteMessages.user_not_found())
        return
    err = person_service.remove_from_favorite(person=person, favorite_username=context.args[0])
    if err:
        update.message.reply_text(RemoveFavoriteMessages.error(err))
        return
    update.message.reply_text(RemoveFavoriteMessages.success())


def move_money_to_card(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 3:
        update.message.reply_text(MoveMoneyToCardMessages.wrong_arguments())
        return
    if not is_int(context.args[1]):
        update.message.reply_text(MoveMoneyToCardMessages.invalid_amount())
        return
    amount = Decimal(context.args[1])
    if amount <= 0:
        update.message.reply_text(MoveMoneyToCardMessages.not_positive_amount())
        return
    person = person_service.get_person_by_id(update.effective_user.id)
    if person is None:
        update.message.reply_text(MoveMoneyToCardMessages.user_not_found())
        return
    transaction_data = TransactionIn(card_from_number=context.args[0], card_to_number=context.args[2], amount=amount)
    err = card_service.move_money(user_from=person, transaction_data=transaction_data)
    if err:
        update.message.reply_text(MoveMoneyToCardMessages.error(err))
        return
    update.message.reply_text(MoveMoneyToCardMessages.success())


def move_money_to_user(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 3:
        update.message.reply_text(MoveMoneyToUserMessages.wrong_arguments())
        return
    if not is_int(context.args[1]):
        update.message.reply_text(MoveMoneyToUserMessages.invalid_amount())
        return
    amount = Decimal(context.args[1])
    if amount <= 0:
        update.message.reply_text(MoveMoneyToUserMessages.not_positive_amount())
        return
    person = person_service.get_person_by_id(update.effective_user.id)
    if person is None:
        update.message.reply_text(MoveMoneyToUserMessages.user_not_found())
        return
    user_to = person_repo.get_person_by_username(context.args[2])
    if user_to is None:
        update.message.reply_text(MoveMoneyToUserMessages.user_with_username_not_found())
        return
    card_to = card_repo.get_first_user_card(user_to)
    if card_to is None:
        update.message.reply_text(MoveMoneyToUserMessages.user_without_cards(context.args[2]))
        return
    transaction_data = TransactionIn(card_from_number=context.args[0], card_to_number=card_to.number, amount=amount)
    err = card_service.move_money(user_from=person, transaction_data=transaction_data)
    if err:
        update.message.reply_text(MoveMoneyToUserMessages.error(err))
        return
    update.message.reply_text(MoveMoneyToUserMessages.success())


def history(update: Update, context: CallbackContext) -> None:
    person = person_repo.get_person_by_id(update.effective_user.id)
    if person is None:
        update.message.reply_text(HistoryMessages.user_not_found())
        return
    transactions = transaction_service.get_user_transactions(
        person=person,
    )
    update.message.reply_text(HistoryMessages.history(transactions))


def history_card(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text(HistoryCardMessages.wrong_arguments())
        return
    person = person_repo.get_person_by_id(update.effective_user.id)
    if person is None:
        update.message.reply_text(HistoryMessages.user_not_found())
        return
    transactions = transaction_service.get_card_history(person=person, card_number=context.args[0])
    if transactions is None:
        update.message.reply_text(HistoryCardMessages.error("No such card"))
        return
    update.message.reply_text(HistoryCardMessages.history(transactions))


def history_account(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text(HistoryAccountMessages.wrong_arguments())
        return
    person = person_repo.get_person_by_id(update.effective_user.id)
    if person is None:
        update.message.reply_text(HistoryAccountMessages.user_not_found())
        return
    transactions = transaction_service.get_bank_account_history(person=person, bank_account_id=context.args[0])
    if transactions is None:
        update.message.reply_text(HistoryAccountMessages.error("No such bank account"))
        return
    update.message.reply_text(HistoryAccountMessages.history(transactions))


def my_senders(update: Update, context: CallbackContext) -> None:
    person = person_repo.get_person_by_id(update.effective_user.id)
    if person is None:
        update.message.reply_text(TargetsListMessages.user_not_found())
        return
    targets, err = transaction_service.get_user_transaction_targets(person)
    if err:
        update.message.reply_text(TargetsListMessages.error(err))
        return
    update.message.reply_text(TargetsListMessages.list(targets))


def login(update: Update, context: CallbackContext):
    tokens, err = auth_service.auth_by_telegram_id(update.effective_user.id)
    if err:
        update.message.reply_text(LoginMessages.error(err))
        return
    update.message.reply_text(LoginMessages.show_tokens(tokens))


def set_password(update: Update, context: CallbackContext):
    if len(context.args) != 1:
        update.message.reply_text(SetPasswordMessages.wrong_arguments())
        return
    person = person_repo.get_person_by_id(update.effective_user.id)
    if person is None:
        update.message.reply_text(SetPasswordMessages.user_not_found())
        return
    success = person_service.set_user_password(person, context.args[0])
    if not success:
        update.message.reply_text(SetPasswordMessages.no_access())
        return
    update.message.reply_text(SetPasswordMessages.success())
