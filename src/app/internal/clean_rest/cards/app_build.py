from ninja import NinjaAPI

from app.internal.clean_rest.bank_account.db.repositories import BankAccountRepository
from app.internal.clean_rest.cards.db.repositories import CardRepository
from app.internal.clean_rest.cards.domain.service import CardService
from app.internal.clean_rest.cards.presentation.handlers import CardHandlers
from app.internal.clean_rest.cards.presentation.routes import add_card_router
from app.internal.clean_rest.transactions.db.repositories import TransactionRepository


def add_cards_api(api: NinjaAPI):
    card_repo = CardRepository()
    bank_account_repo = BankAccountRepository()
    transaction_repo = TransactionRepository()
    card_service = CardService(card_repo, transaction_repo, bank_account_repo)
    card_handler = CardHandlers(card_service)
    add_card_router(api, card_handler)
