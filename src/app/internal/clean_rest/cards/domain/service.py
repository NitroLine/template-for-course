from decimal import Decimal
from typing import List, Optional

from app.internal.clean_rest.bank_account.db.models import BankAccount
from app.internal.clean_rest.bank_account.db.repositories import IBankAccountRepository
from app.internal.clean_rest.cards.db.repositories import ICardRepository
from app.internal.clean_rest.cards.domain.entities import CardIn, CardOut, TransactionIn
from app.internal.clean_rest.errors.states import (
    CARD_NOT_EXIST,
    MOVE_MONEY_TO_YOURSELF,
    NO_SUCH_USER_BANK_ACCOUNT,
    NO_SUCH_USER_CARD,
    NOT_ENOUGH_MONEY,
)
from app.internal.clean_rest.person.db.models import Person
from app.internal.clean_rest.transactions.db.repositories import ITransactionRepository


class CardService:
    def __init__(
        self,
        card_repo: ICardRepository,
        transaction_repo: ITransactionRepository,
        bank_account_repo: IBankAccountRepository,
    ):
        self._card_repo = card_repo
        self._transaction_repo = transaction_repo
        self._bank_account_repo = bank_account_repo

    def move_money(self, user_from: Person, transaction_data: TransactionIn) -> Optional[str]:
        card_from = self._card_repo.get_user_card(user_from, transaction_data.card_from_number)
        if card_from is None:
            return NO_SUCH_USER_CARD
        if card_from.amount - transaction_data.amount < 0:
            return NOT_ENOUGH_MONEY
        card_to = self._card_repo.get_card_by_number(transaction_data.card_to_number)
        if card_to is None:
            return CARD_NOT_EXIST
        if card_to == card_from:
            return MOVE_MONEY_TO_YOURSELF
        err = self._card_repo.move_money(card_from, card_to, transaction_data.amount)
        if err:
            return err
        self._transaction_repo.create_transaction(card_from, card_to, transaction_data.amount)

    def create_user_card(self, person: Person, card_data: CardIn) -> (Optional[CardOut], Optional[str]):
        bank_account = self._bank_account_repo.get_user_bank_account(person, card_data.bank_account_id)
        if bank_account is None:
            return None, NO_SUCH_USER_BANK_ACCOUNT
        return CardOut.from_orm(self._card_repo.create_card(bank_account, card_data.amount)), None

    def get_bank_account_balance(self, account: BankAccount) -> Decimal:
        return self._card_repo.get_bank_account_balance(account)

    def get_card_balance(self, person: Person, card_number: int) -> (Decimal, Optional[str]):
        return self._card_repo.get_card_balance(person, card_number)

    def get_all_user_cards(self, person: Person) -> [CardOut]:
        return list(map(lambda x: CardOut.from_orm(x), self._card_repo.get_all_user_cards(person)))
