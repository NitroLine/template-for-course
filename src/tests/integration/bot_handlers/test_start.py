import pytest

from app.internal.bot.handlers import start
from app.internal.bot.messages import StartMessages
from app.internal.clean_rest.person.db.models import Person


@pytest.mark.integration
@pytest.mark.django_db
def test_start_handler(test_update, test_context):
    start(test_update, test_context)
    assert Person.objects.filter(telegram_id=test_update.effective_user.id).exists()
    test_update.message.reply_text.assert_called_once_with(StartMessages.hello(test_update.effective_user.first_name))
