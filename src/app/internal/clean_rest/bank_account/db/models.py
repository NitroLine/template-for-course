from django.db import models

from app.internal.clean_rest.person.db.models import Person


class BankAccount(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    id = models.AutoField(auto_created=True, primary_key=True)

    def __str__(self):
        return f"Bank Account {self.id}"
