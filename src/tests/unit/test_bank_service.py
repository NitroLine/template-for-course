import pytest

from app.internal.clean_rest.bank_account.db.models import BankAccount
from app.internal.clean_rest.cards.db.models import Card
from app.internal.clean_rest.person.db.models import Person
from tests.tempservice.services.bank_service import (
    get_bank_account_balance,
    get_card_balance,
    get_user_balance,
    move_money,
    move_money_on_telegram_username,
)
from tests.tempservice.services.states import CARD_NOT_EXIST, NO_SUCH_USER_CARD, NOT_ENOUGH_MONEY, PERSON_NOT_EXIST

NOT_EXIST_ID = 333451


@pytest.mark.unit
class TestBankAccountService:
    @pytest.mark.django_db
    def test_get_user_balance_when_exist(self, test_user_with_account_and_card: (Person, BankAccount, Card)):
        user, bank_account, card = test_user_with_account_and_card
        balance, err = get_user_balance(user.telegram_id)
        assert err is None
        assert balance == 5000

    @pytest.mark.django_db
    def test_get_user_balance_when_not_exist(self):
        balance, err = get_user_balance(NOT_EXIST_ID)
        assert err == PERSON_NOT_EXIST
        assert balance == 0

    @pytest.mark.django_db
    def test_get_bank_account_balance_when_exist(self, test_user_with_account_and_card: (Person, BankAccount, Card)):
        user, bank_account, card = test_user_with_account_and_card
        balance = get_bank_account_balance(bank_account.id)
        assert balance == 5000

    @pytest.mark.django_db
    def test_get_bank_account_balance_when_not_exist(self):
        balance = get_bank_account_balance(NOT_EXIST_ID)
        assert balance == 0

    @pytest.mark.django_db
    def test_get_card_balance_when_your_card(self, test_user_with_account_and_card: (Person, BankAccount, Card)):
        user, bank_account, card = test_user_with_account_and_card
        balance, err = get_card_balance(user.telegram_id, card.number)
        assert err is None
        assert balance == 5000

    @pytest.mark.django_db
    def test_get_card_balance_when_not_your_card(
        self, test_user_with_account_and_card: (Person, BankAccount, Card), test_user: Person
    ):
        user, bank_account, card = test_user_with_account_and_card
        balance, err = get_card_balance(test_user.telegram_id, card.number)
        assert err == NO_SUCH_USER_CARD
        assert balance == 0

    @pytest.mark.django_db
    def test_get_card_balance_when_user_not_exist(self, test_user_with_account_and_card: (Person, BankAccount, Card)):
        user, bank_account, card = test_user_with_account_and_card
        balance, err = get_card_balance(NOT_EXIST_ID, card.number)
        assert err == PERSON_NOT_EXIST
        assert balance == 0

    @pytest.mark.django_db
    def test_move_money(self, test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card)):
        user, bank_account, card1, card2 = test_user_with_account_and_two_cards
        move_amount = 200
        err = move_money(user.telegram_id, card1.number, card2.number, move_amount)
        assert err is None
        assert get_card_balance(user.telegram_id, card1.number)[0] == card1.amount - move_amount
        assert get_card_balance(user.telegram_id, card2.number)[0] == card2.amount + move_amount

    @pytest.mark.django_db
    def test_move_money_when_not_enough_money(
        self, test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card)
    ):
        user, bank_account, card1, card2 = test_user_with_account_and_two_cards
        move_amount = 10000
        err = move_money(user.telegram_id, card1.number, card2.number, move_amount)
        assert err == NOT_ENOUGH_MONEY
        assert get_card_balance(user.telegram_id, card1.number)[0] == card1.amount
        assert get_card_balance(user.telegram_id, card2.number)[0] == card2.amount

    @pytest.mark.django_db
    def test_move_money_when_not_your_card(
        self,
        test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card),
        test_user_with_account_and_card: (Person, BankAccount, Card),
    ):
        user, bank_account, card1, card2 = test_user_with_account_and_two_cards
        another_user, another_account, another_card = test_user_with_account_and_card
        move_amount = 200
        err = move_money(user.telegram_id, another_card.number, card2.number, move_amount)
        assert err == NO_SUCH_USER_CARD
        assert get_card_balance(user.telegram_id, card2.number)[0] == card2.amount
        assert get_card_balance(another_user.telegram_id, another_card.number)[0] == another_card.amount

    @pytest.mark.django_db
    def test_move_money_when_not_valid_from(
        self, test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card)
    ):
        user, bank_account, card1, card2 = test_user_with_account_and_two_cards
        move_amount = 200
        err = move_money(user.telegram_id, NOT_EXIST_ID, card2.number, move_amount)
        assert err == NO_SUCH_USER_CARD
        assert get_card_balance(user.telegram_id, card1.number)[0] == card1.amount
        assert get_card_balance(user.telegram_id, card2.number)[0] == card2.amount

    @pytest.mark.django_db
    def test_move_money_when_not_valid_to(
        self, test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card)
    ):
        user, bank_account, card1, card2 = test_user_with_account_and_two_cards
        move_amount = 200
        err = move_money(user.telegram_id, card1.number, NOT_EXIST_ID, move_amount)
        assert err == CARD_NOT_EXIST
        assert get_card_balance(user.telegram_id, card1.number)[0] == card1.amount
        assert get_card_balance(user.telegram_id, card2.number)[0] == card2.amount

    @pytest.mark.django_db
    def test_move_money_when_not_valid_user(
        self, test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card)
    ):
        user, bank_account, card1, card2 = test_user_with_account_and_two_cards
        move_amount = 200
        err = move_money(NOT_EXIST_ID, card1.number, card2.number, move_amount)
        assert err == PERSON_NOT_EXIST
        assert get_card_balance(user.telegram_id, card1.number)[0] == card1.amount
        assert get_card_balance(user.telegram_id, card2.number)[0] == card2.amount

    @pytest.mark.django_db
    def test_move_money_on_telegram_username(
        self,
        test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card),
        test_user_with_account_and_card: (Person, BankAccount, Card),
    ):
        user, bank_account, card1, card2 = test_user_with_account_and_two_cards
        another_user, another_account, another_card = test_user_with_account_and_card
        move_amount = 200
        err = move_money_on_telegram_username(user.telegram_id, card1.number, another_user.username, move_amount)
        assert err is None
        assert get_card_balance(user.telegram_id, card1.number)[0] == card1.amount - move_amount
        assert get_card_balance(another_user.telegram_id, another_card.number)[0] == another_card.amount + move_amount

    @pytest.mark.django_db
    def test_move_money_on_telegram_username_when_not_exist(
        self, test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card)
    ):
        user, bank_account, card1, card2 = test_user_with_account_and_two_cards
        move_amount = 200
        err = move_money_on_telegram_username(user.telegram_id, card1.number, "TESTER_USER_NOT_EXIST", move_amount)
        assert err == PERSON_NOT_EXIST
        assert get_card_balance(user.telegram_id, card1.number)[0] == card1.amount

    @pytest.mark.django_db
    def test_move_money_on_telegram_username_when_user_without_card(
        self, test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card), test_user: Person
    ):
        user, bank_account, card1, card2 = test_user_with_account_and_two_cards
        move_amount = 200
        err = move_money_on_telegram_username(user.telegram_id, card1.number, test_user.username, move_amount)
        assert err == CARD_NOT_EXIST
        assert get_card_balance(user.telegram_id, card1.number)[0] == card1.amount

    @pytest.mark.django_db
    def test_move_money_on_telegram_username_when_not_enough_money(
        self,
        test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card),
        test_user_with_account_and_card: (Person, BankAccount, Card),
    ):
        user, bank_account, card1, card2 = test_user_with_account_and_two_cards
        another_user, another_account, another_card = test_user_with_account_and_card
        move_amount = 100000
        err = move_money_on_telegram_username(user.telegram_id, card1.number, another_user.username, move_amount)
        assert err is NOT_ENOUGH_MONEY
        assert get_card_balance(user.telegram_id, card1.number)[0] == card1.amount
        assert get_card_balance(another_user.telegram_id, another_card.number)[0] == another_card.amount
