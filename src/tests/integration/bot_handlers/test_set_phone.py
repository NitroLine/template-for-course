import pytest

from app.internal.bot.handlers import set_phone
from app.internal.bot.messages import SetPhoneMessages
from app.internal.clean_rest.person.db.models import Person


@pytest.mark.integration
class TestSetPhoneHandler:
    @pytest.mark.django_db
    def test_set_phone_no_args(self, test_update, test_context, test_user):
        set_phone(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(SetPhoneMessages.wrong_arguments())

    @pytest.mark.django_db
    def test_set_phone_invalid_format(self, test_update, test_context, test_user):
        test_context.args = ["00"]
        set_phone(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(SetPhoneMessages.wrong_format())

    @pytest.mark.django_db
    def test_set_phone_valid(self, test_update, test_context, test_user):
        test_context.args = ["+79221011013"]
        set_phone(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(SetPhoneMessages.success())
        assert str(Person.objects.get(telegram_id=test_update.effective_user.id).phone_number) == test_context.args[0]
