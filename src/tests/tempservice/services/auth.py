from datetime import datetime
from typing import Dict, Optional, Tuple

import jwt
from django.utils.timezone import now

from app.internal.clean_rest.auth.db.models import IssuedToken
from app.internal.clean_rest.person.db.models import Person
from config.settings import JWT_ALGORITHM, JWT_SECRET, TOKENS_TTL
from tests.tempservice.services.states import INVALID_PASSWORD, PERSON_NOT_EXIST, TOKEN_REVOKED
from tests.tempservice.services.user_service import get_person_by_id, get_person_by_username, hash_user_password


class TokenTypes:
    REFRESH = "refresh"
    ACCESS = "access"


def generate_token(person_id: int, token_type: str) -> str:
    payload = {"type": token_type, "person_id": person_id, "created_at": now().timestamp()}
    return jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)


def decode_token(token: str) -> Optional[Dict[str, str]]:
    if token is None:
        return None
    try:
        return jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
    except jwt.PyJWTError:
        return None


def auth_by_telegram_id(telegram_id: int) -> (Optional[Tuple[str, str]], Optional[str]):
    person = get_person_by_id(telegram_id)
    if person is None:
        return None, PERSON_NOT_EXIST
    return generate_tokens(person)


def auth_by_password(username: str, password: str) -> (Optional[Tuple[str, str]], Optional[str]):
    person = get_person_by_username(username)
    if person is None:
        return None, PERSON_NOT_EXIST
    hashed_pass = hash_user_password(password)
    if person.password.tobytes() != hashed_pass:
        return None, INVALID_PASSWORD
    return generate_tokens(person)


def generate_tokens(person: Person) -> (Optional[Tuple[str, str]], Optional[str]):
    IssuedToken.objects.filter(user=person).delete()
    access = generate_token(person.id, TokenTypes.ACCESS)
    refresh = generate_token(person.id, TokenTypes.REFRESH)
    IssuedToken.objects.create(user=person, jti=refresh)
    return access, refresh


def refresh_tokens(issued_token: IssuedToken) -> (Optional[Tuple[str, str]], Optional[str]):
    if issued_token.revoked:
        IssuedToken.objects.filter(user=issued_token.user).update(revoked=True)
        return None, TOKEN_REVOKED
    access = generate_token(issued_token.user.telegram_id, TokenTypes.ACCESS)
    refresh = generate_token(issued_token.user.telegram_id, TokenTypes.REFRESH)
    IssuedToken.objects.create(user=issued_token.user, jti=refresh)
    IssuedToken.objects.filter(jti=issued_token.jti).update(revoked=True)
    return (access, refresh), None


def get_issued_token(ref_token: str) -> Optional[IssuedToken]:
    return IssuedToken.objects.filter(jti=ref_token).select_related("user").first()


def is_token_alive(token: dict, token_type: str):
    created = float(token.get("created_at"))
    return created >= (datetime.now() - TOKENS_TTL[token_type]).timestamp()


def validate_refresh_token(token: str) -> (Optional[IssuedToken], Optional[str]):
    issued_token = get_issued_token(token)
    if issued_token is None:
        return None, "Unknown refresh token"
    payload = decode_token(token)
    if payload is None:
        return None, "Invalid token"
    if payload.get("type", None) != TokenTypes.REFRESH:
        return None, "Invalid token type"
    if not is_token_alive(payload, TokenTypes.REFRESH):
        return None, "Token expired"
    return issued_token, None
