FROM python:3.10-slim-buster

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/app
RUN pip install --upgrade pip &&  \
     pip install pipenv
COPY Pipfile* ./
RUN pipenv install --system --deploy

COPY . .