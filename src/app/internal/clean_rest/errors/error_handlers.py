from django.http import HttpRequest
from ninja import NinjaAPI

from app.internal.clean_rest.errors.errors import (
    BadRequestException,
    NoAccessException,
    NoPhoneNumberException,
    NotExistException,
    UnauthorizedException,
)


def phone_number_not_set_handler(request: HttpRequest, exc: NoPhoneNumberException, api: NinjaAPI):
    return api.create_response(request, {"error": f"No permission: {exc.message}"}, status=403)


def not_exist_handler(request: HttpRequest, exc: NotExistException, api: NinjaAPI):
    return api.create_response(request, {"error": f"Not exist: {exc.message}"}, status=400)


def authorization_exception_handler(request: HttpRequest, exc: UnauthorizedException, api: NinjaAPI):
    return api.create_response(request, {"error": exc.message}, status=401)


def no_access_exception_handler(request: HttpRequest, exc: NoAccessException, api: NinjaAPI):
    return api.create_response(request, {"error": f"No access: {exc.message}"}, status=403)


def bad_request_handler(request: HttpRequest, exc: BadRequestException, api: NinjaAPI):
    return api.create_response(request, {"error": exc.message}, status=400)
