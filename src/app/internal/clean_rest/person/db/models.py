from django.core.validators import RegexValidator
from django.db import models

from config.settings import PHONE_REGEX


class Person(models.Model):
    id = models.AutoField(auto_created=True, primary_key=True)
    first_name = models.CharField(max_length=255)
    telegram_id = models.IntegerField(unique=True)
    username = models.CharField(max_length=120, blank=True, null=True)
    phone_number = models.CharField(
        max_length=16,
        validators=[RegexValidator(regex=PHONE_REGEX)],
        blank=True,
    )
    favorites = models.ManyToManyField("Person")
    password = models.BinaryField(blank=True, null=True)

    def __str__(self):
        return f"Person({self.telegram_id}) : First Name = {self.first_name}; Username = {self.username}; Phone number = {self.phone_number};"
