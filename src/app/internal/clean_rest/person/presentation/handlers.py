import re

from ninja import Body

from app.internal.clean_rest.default.responce import SuccessResponse
from app.internal.clean_rest.errors.errors import BadRequestException, NoPhoneNumberException
from app.internal.clean_rest.person.domain.entities import UserOut
from app.internal.clean_rest.person.domain.services import PersonService
from config.settings import PHONE_REGEX


class PersonHandlers:
    def __init__(self, person_service: PersonService):
        self.person_service = person_service

    def request_person_data(self, request) -> UserOut:
        user_out = self.person_service.request_person_data(request.user)
        if user_out is None:
            raise NoPhoneNumberException()
        return user_out

    def set_phone_number(self, request, phone_number: str = Body(...)):
        if not re.match(PHONE_REGEX, phone_number):
            raise BadRequestException("Invalid phone number")
        self.person_service.set_phone_on_person(request.user, phone_number)
        return SuccessResponse(success=True)

    def add_to_favorite(self, request, favorite_username: str = Body(...)) -> SuccessResponse:
        err = self.person_service.add_to_favorite(request.user, favorite_username)
        if err is not None:
            raise BadRequestException(err)
        return SuccessResponse(success=True)

    def remove_from_favorite(self, request, favorite_username: str = Body(...)) -> SuccessResponse:
        err = self.person_service.remove_from_favorite(request.user, favorite_username)
        if err is not None:
            raise BadRequestException(err)
        return SuccessResponse(success=True)

    def get_user_favorites(self, request) -> [str]:
        return self.person_service.get_user_favorites(request.user)
