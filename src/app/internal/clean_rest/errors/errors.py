from app.internal.clean_rest.errors.states import PERSON_NOT_EXIST, PERSON_NOT_SET_PHONE


class NoPhoneNumberException(Exception):
    def __init__(self):
        super().__init__()
        self.message = PERSON_NOT_SET_PHONE


class NotExistException(Exception):
    def __init__(self):
        super().__init__()
        self.message = PERSON_NOT_EXIST


class BadRequestException(Exception):
    def __init__(self, message):
        super().__init__()
        self.message = message


class UnauthorizedException(Exception):
    def __init__(self, message):
        super().__init__()
        self.message = message


class NoAccessException(Exception):
    def __init__(self, message):
        super().__init__()
        self.message = message
