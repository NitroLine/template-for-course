from django.contrib import admin

from app.internal.clean_rest.cards.db.models import Card


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    list_display = ("number", "amount", "bank_account", "owner")

    def owner(self, card: Card):
        return card.bank_account.person
