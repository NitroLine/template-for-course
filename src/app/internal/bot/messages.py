import json


class Messages:
    @staticmethod
    def error(error_message) -> str:
        return f"Error: {error_message}"

    @classmethod
    def wrong_arguments(cls):
        return "Wrong argument number. " + cls.help()

    @staticmethod
    def help():
        return ""

    @staticmethod
    def user_not_found():
        return "Phone number not saved. Use /start first"


class StartMessages(Messages):
    @staticmethod
    def hello(name):
        return f"Hello {name}"


class SetPhoneMessages(Messages):
    @staticmethod
    def help():
        return "Use /set_number <phone_number>."

    @staticmethod
    def wrong_format():
        return "Wrong format of phone"

    @staticmethod
    def success():
        return "Phone number saved"


class BalanceMessages(Messages):
    @staticmethod
    def show_balance(balance):
        return f"Your balance: {balance}"


class LoginMessages(Messages):
    @staticmethod
    def show_tokens(tokens):
        return f"Your access token: {tokens[0]} \n Your refresh tokens: {tokens[1]}"


class MeMessages(Messages):
    @staticmethod
    def me_info(info: dict):
        return json.dumps(info, indent=2, ensure_ascii=False)

    @staticmethod
    def no_access():
        return "Unknown user or you not set phone number"


class SetPasswordMessages(Messages):
    @staticmethod
    def no_access():
        return "Unknown user or you not set phone number"

    @staticmethod
    def success():
        return "Password saved"


class TargetsListMessages(Messages):
    @staticmethod
    def list(targets_list: list):
        return "You has been interact with \n" + json.dumps(targets_list, indent=2, ensure_ascii=False)


class HistoryMessages(Messages):
    @staticmethod
    def history(transactions) -> str:
        return "Your transactions: \n" + json.dumps(transactions, indent=1, ensure_ascii=False)


class HistoryCardMessages(HistoryMessages):
    @staticmethod
    def help():
        return "Use /history_card <card_number>."


# TODO: тесты
# TODO: датаклассы
class HistoryAccountMessages(HistoryMessages):
    @staticmethod
    def help():
        return "Use /history_account <account_id>."


class CardBalanceMessages(Messages):
    @staticmethod
    def help():
        return " Use /card_balance <card_number>."

    @staticmethod
    def success(amount):
        return f"Card balance: {amount}"


class NewFavoriteMessages(Messages):
    @staticmethod
    def help():
        return "Use /new_favorite <telegram_username>."

    @staticmethod
    def success():
        return "Successful"


class RemoveFavoriteMessages(Messages):
    @staticmethod
    def help():
        return "Use /remove_favorite <telegram_username>."

    @staticmethod
    def success():
        return "Successful"


class MoveMoneyMessages(Messages):
    @staticmethod
    def invalid_amount():
        return "Invalid amount value to move"

    @staticmethod
    def not_positive_amount():
        return "Amount value to move must be positive"

    @staticmethod
    def move_to_yourself():
        return "Cant move money to yourself"

    @staticmethod
    def success():
        return "Successful"


class MoveMoneyToCardMessages(MoveMoneyMessages):
    @staticmethod
    def help():
        return "Use /move_money_to_card <FROM_card_number> <amount> <TO_card_number>."


class MoveMoneyToUserMessages(MoveMoneyMessages):
    @staticmethod
    def help():
        return "Use /move_money_to_card <FROM_card_number> <amount> <TO_telegram_username>."

    @staticmethod
    def user_with_username_not_found():
        return "No user found with such username"

    @staticmethod
    def user_without_cards(username):
        return f"User {username} not have any cards "
