from typing import List

from ninja import NinjaAPI, Router

from app.internal.clean_rest.bank_account.presentations.entities import BankAccountOut
from app.internal.clean_rest.bank_account.presentations.handlers import BankAccountHandlers


def add_bank_account_router(api: NinjaAPI, handlers: BankAccountHandlers):
    account_router = get_bank_account_router(handlers)
    api.add_router("/bank_account", account_router)


def get_bank_account_router(bank_account_handlers: BankAccountHandlers):
    router = Router(tags=["bank_accounts"])

    router.add_api_operation(
        "/",
        ["GET"],
        bank_account_handlers.get_bank_accounts,
        response={200: List[BankAccountOut]},
    )
    router.add_api_operation(
        "/",
        ["POST"],
        bank_account_handlers.create_bank_account,
        response={200: BankAccountOut},
    )

    return router
