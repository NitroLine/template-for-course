from ninja import Schema


class CredentialsSchema(Schema):
    username: str
    password: str


class JWTTokensSchema(Schema):
    access_token: str
    refresh_token: str
