from datetime import datetime
from typing import Dict, Optional, Tuple, Union

import jwt
from django.utils.timezone import now

from app.internal.clean_rest.auth.db.models import IssuedToken
from app.internal.clean_rest.person.db.models import Person
from config.settings import JWT_ALGORITHM, JWT_SECRET, TOKENS_TTL
from tests.tempservice.services.states import TOKEN_REVOKED


class TokenTypes:
    REFRESH = "refresh"
    ACCESS = "access"


class IAuthRepository:
    def generate_token(self, person_id: int, token_type: str) -> str:
        ...

    def decode_token(self, token: str) -> Optional[Dict[str, str]]:
        ...

    def get_issued_token(self, ref_token: str) -> Optional[IssuedToken]:
        ...

    def refresh_tokens(self, issued_token: IssuedToken) -> (Optional[Tuple[str, str]], Optional[str]):
        ...

    def generate_new_tokens(self, person: Person) -> Tuple[str, str]:
        ...

    def is_token_alive(self, payload: dict, token_type: str) -> bool:
        ...


class JWTAuthRepository(IAuthRepository):
    def is_token_alive(self, payload: dict, token_type: str) -> bool:
        created = float(payload.get("created_at"))
        return created >= (datetime.now() - TOKENS_TTL[token_type]).timestamp()

    def generate_token(self, person_id: int, token_type: str) -> str:
        payload = {"type": token_type, "person_id": person_id, "created_at": now().timestamp()}
        return jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)

    def refresh_tokens(self, issued_token: IssuedToken) -> (Optional[Tuple[str, str]], Optional[str]):
        if issued_token.revoked:
            IssuedToken.objects.filter(user=issued_token.user).update(revoked=True)
            return None, TOKEN_REVOKED
        access = self.generate_token(issued_token.user.telegram_id, TokenTypes.ACCESS)
        refresh = self.generate_token(issued_token.user.telegram_id, TokenTypes.REFRESH)
        IssuedToken.objects.create(user=issued_token.user, jti=refresh)
        IssuedToken.objects.filter(jti=issued_token.jti).update(revoked=True)
        return (access, refresh), None

    def generate_new_tokens(self, person: Person) -> Tuple[str, str]:
        IssuedToken.objects.filter(user=person).delete()
        access = self.generate_token(person.telegram_id, TokenTypes.ACCESS)
        refresh = self.generate_token(person.telegram_id, TokenTypes.REFRESH)
        IssuedToken.objects.create(user=person, jti=refresh)
        return access, refresh

    def get_issued_token(self, ref_token: str) -> Optional[IssuedToken]:
        return IssuedToken.objects.filter(jti=ref_token).select_related("user").first()

    def decode_token(self, token: str) -> Optional[Dict[str, Union[str, int]]]:
        if token is None:
            return None
        try:
            return jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
        except jwt.PyJWTError:
            return None
