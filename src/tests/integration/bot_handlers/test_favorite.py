import pytest

from app.internal.bot.handlers import new_favorite, remove_favorite
from app.internal.bot.messages import NewFavoriteMessages, RemoveFavoriteMessages
from app.internal.clean_rest.person.db.models import Person


@pytest.mark.integration
class TestNewFavoriteHandler:
    @pytest.mark.django_db
    def test_new_favorite(self, test_update, test_context, test_user_without_phone: Person, test_user: Person):
        test_update.effective_user.id = test_user.telegram_id
        test_context.args = [test_user_without_phone.username]
        new_favorite(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(NewFavoriteMessages.success())

    @pytest.mark.django_db
    def test_new_favorite_no_args(self, test_update, test_context, test_user: Person):
        test_update.effective_user.id = test_user.telegram_id
        new_favorite(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(NewFavoriteMessages.wrong_arguments())


@pytest.mark.integration
class TestRemoveFavoriteHandler:
    @pytest.mark.django_db
    def test_remove_favorite_empty(self, test_update, test_context, test_user_without_phone: Person, test_user: Person):
        test_update.effective_user.id = test_user.telegram_id
        test_context.args = [test_user_without_phone.username]
        new_favorite(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(RemoveFavoriteMessages.success())

    @pytest.mark.django_db
    def test_remove_favorite(self, test_update, test_context, test_user_with_favorite_list: (Person, Person)):
        first_user, second_user = test_user_with_favorite_list
        test_update.effective_user.id = first_user.telegram_id
        test_context.args = [second_user.username]
        remove_favorite(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(RemoveFavoriteMessages.success())

    @pytest.mark.django_db
    def test_remove_favorite_no_args(self, test_update, test_context, test_user: Person):
        test_update.effective_user.id = test_user.telegram_id
        remove_favorite(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(RemoveFavoriteMessages.wrong_arguments())
