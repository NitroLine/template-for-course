from typing import List

from ninja import NinjaAPI, Router

from app.internal.clean_rest.default.responce import ErrorResponse, SuccessResponse
from app.internal.clean_rest.person.domain.entities import UserOut
from app.internal.clean_rest.person.presentation.handlers import PersonHandlers


def add_users_router(api: NinjaAPI, person_handlers: PersonHandlers):
    users_router = get_users_router(person_handlers)
    api.add_router("/users", users_router)


def get_users_router(user_handlers: PersonHandlers):
    router = Router(tags=["users"])

    router.add_api_operation(
        "/me",
        ["GET"],
        user_handlers.request_person_data,
        response={200: UserOut, 400: ErrorResponse},
    )

    router.add_api_operation(
        "/set_phone",
        ["PATCH"],
        user_handlers.set_phone_number,
        response={200: SuccessResponse, 400: ErrorResponse},
    )

    router.add_api_operation(
        "/favourite_users",
        ["GET"],
        user_handlers.get_user_favorites,
        response={200: List[str]},
    )

    router.add_api_operation(
        "/favourite_users",
        ["POST"],
        user_handlers.add_to_favorite,
        response={200: SuccessResponse, 400: ErrorResponse},
    )

    router.add_api_operation(
        "/favourite_users",
        ["DELETE"],
        user_handlers.remove_from_favorite,
        response={200: SuccessResponse, 400: ErrorResponse},
    )

    return router
