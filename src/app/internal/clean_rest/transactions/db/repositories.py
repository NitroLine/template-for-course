from decimal import Decimal
from typing import Dict, Optional, Union

from django.db.models import F, Q, QuerySet

from app.internal.clean_rest.bank_account.db.models import BankAccount
from app.internal.clean_rest.cards.db.models import Card
from app.internal.clean_rest.person.db.models import Person
from app.internal.clean_rest.transactions.db.models import Transaction


class ITransactionRepository:
    def create_transaction(self, card_from: Card, card_to: Card, amount: Decimal) -> Transaction:
        ...

    def get_user_transaction_targets(self, person: Person) -> QuerySet[str]:
        ...

    def get_user_transactions(self, person_id: int) -> QuerySet[Dict[str, Union[str, int]]]:
        ...

    def get_card_transactions(self, person_id: int, card: Card) -> QuerySet[Dict[str, Union[str, int]]]:
        ...

    def get_bank_account_transactions(
        self, person_id: int, bank_account: BankAccount
    ) -> QuerySet[Dict[str, Union[str, int]]]:
        ...


class TransactionRepository(ITransactionRepository):
    def create_transaction(self, card_from: Card, card_to: Card, amount: Decimal) -> Transaction:
        return Transaction.objects.create(card_from=card_from, card_to=card_to, amount=amount)

    def get_user_transaction_targets(self, person_id: int) -> QuerySet[str]:
        usernames_to = Transaction.objects.filter(card_to__bank_account__person_id=person_id).values_list(
            "card_from__bank_account__person__username", flat=True
        )
        usernames_from = Transaction.objects.filter(card_from__bank_account__person_id=person_id).values_list(
            "card_to__bank_account__person__username", flat=True
        )
        targets = usernames_from.union(usernames_to).all()
        return targets

    def get_user_transactions(self, person_id: int) -> QuerySet[Dict[str, Union[str, int]]]:
        transactions = (
            Transaction.objects.filter(
                Q(card_from__bank_account__person_id=person_id) | Q(card_to__bank_account__person_id=person_id)
            )
            .annotate(
                card_number_from=F("card_from__number"),
                card_number_to=F("card_to__number"),
                username_from=F("card_from__bank_account__person__username"),
                user_to=F("card_to__bank_account__person__username"),
            )
            .values(
                "date",
                "card_number_from",
                "card_number_to",
                "amount",
                "username_from",
                "user_to",
            )
            .all()
        )
        return transactions

    def get_card_transactions(self, person_id: int, card: Card) -> QuerySet[Dict[str, Union[str, int]]]:
        transactions = (
            Transaction.objects.filter(
                Q(card_from__bank_account__person_id=person_id, card_from__number=card)
                | Q(card_to__bank_account__person_id=person_id, card_to__number=card)
            )
            .annotate(
                card_number_from=F("card_from__number"),
                card_number_to=F("card_to__number"),
                username_from=F("card_from__bank_account__person__username"),
                user_to=F("card_to__bank_account__person__username"),
            )
            .values(
                "date",
                "card_number_from",
                "card_number_to",
                "amount",
                "username_from",
                "user_to",
            )
            .all()
        )
        return transactions

    def get_bank_account_transactions(
        self, person_id: int, bank_account: BankAccount
    ) -> QuerySet[Dict[str, Union[str, int]]]:
        transactions = (
            Transaction.objects.filter(
                Q(card_from__bank_account__person_id=person_id, card_from__bank_account__id=bank_account)
                | Q(card_to__bank_account__person_id=person_id, card_to__bank_account__id=bank_account)
            )
            .annotate(
                card_number_from=F("card_from__number"),
                card_number_to=F("card_to__number"),
                username_from=F("card_from__bank_account__person__username"),
                user_to=F("card_to__bank_account__person__username"),
            )
            .values(
                "date",
                "card_number_from",
                "card_number_to",
                "amount",
                "username_from",
                "user_to",
            )
            .all()
        )
        return transactions
