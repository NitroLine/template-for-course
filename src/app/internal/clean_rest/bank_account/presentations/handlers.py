from app.internal.clean_rest.bank_account.db.repositories import IBankAccountRepository
from app.internal.clean_rest.bank_account.presentations.entities import BankAccountOut
from app.internal.clean_rest.person.db.models import Person


class BankAccountHandlers:
    def __init__(self, bank_account_repo: IBankAccountRepository):
        self._account_repo = bank_account_repo

    def create_bank_account(self, request) -> BankAccountOut:
        return BankAccountOut.from_orm(self._account_repo.create_bank_account(request.user))

    def get_bank_accounts(self, request):
        return list(map(BankAccountOut.from_orm, self._account_repo.get_user_bank_accounts(request.user)))
