from typing import Optional

from app.internal.clean_rest.person.db.models import Person


class IPersonRepository:
    def create_person(self, telegram_id: int, first_name: str, username: str) -> (Person, bool):
        ...

    def get_person_by_id(self, telegram_id: int) -> Optional[Person]:
        ...

    def get_person_by_username(self, username: str) -> Optional[Person]:
        ...

    def get_person_id(self, telegram_id: int) -> Optional[str]:
        ...

    def set_phone_on_person(self, person: Person, phone: str) -> None:
        ...

    def set_user_password(self, person: Person, password_hash: bytes) -> None:
        ...

    def add_to_favorite(self, person: Person, favorite_person: Person) -> None:
        ...

    def remove_from_favorite(self, person: Person, favorite_person: Person) -> None:
        ...

    def get_user_favorites(self, person: Person) -> [str]:
        ...


class PersonRepository(IPersonRepository):
    def create_person(self, telegram_id: int, first_name: str, username: str) -> (Person, bool):
        return Person.objects.get_or_create(
            telegram_id=telegram_id,
            defaults={
                "username": username,
                "first_name": first_name,
            },
        )

    def get_person_by_id(self, telegram_id: int) -> Optional[Person]:
        return Person.objects.filter(telegram_id=telegram_id).first()

    def get_person_id(self, telegram_id: int) -> Optional[int]:
        result = Person.objects.filter(telegram_id=telegram_id).values("id").first()
        if result is not None:
            return result["id"]

    def get_person_by_username(self, username: str) -> Optional[Person]:
        return Person.objects.filter(username=username).first()

    def set_phone_on_person(self, person: Person, phone: str) -> None:
        person.phone_number = phone
        person.save()

    def set_user_password(self, person: Person, password_hash: bytes) -> None:
        person.password = password_hash
        person.save()

    def add_to_favorite(self, person: Person, favorite_person: Person) -> None:
        person.favorites.add(favorite_person)
        person.save()

    def remove_from_favorite(self, person: Person, favorite_person: Person) -> None:
        person.favorites.remove(favorite_person)
        person.save()

    def get_user_favorites(self, person: Person) -> [str]:
        return list(person.favorites.all().values_list("username", flat=True))
