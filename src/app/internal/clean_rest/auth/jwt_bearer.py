from django.http import HttpRequest
from ninja.security import HttpBearer

from app.internal.clean_rest.auth.db.repositories import JWTAuthRepository, TokenTypes
from app.internal.clean_rest.person.db.repositories import PersonRepository

user_repo = PersonRepository()
auth_repo = JWTAuthRepository()


class HTTPJWTAuth(HttpBearer):
    def authenticate(self, request: HttpRequest, token):
        if not token:
            print("No token")
            return None
        payload = auth_repo.decode_token(token)
        if payload is None:
            print("Cant decode")
            return None
        print(payload.get("type"), TokenTypes.ACCESS)
        if payload.get("type") != TokenTypes.ACCESS:
            print("type wrong")
            return None
        if not auth_repo.is_token_alive(payload, TokenTypes.ACCESS):
            print("Token not alive")
            return None
        user = user_repo.get_person_by_id(payload["person_id"])
        request.user = user
        print("Authed")
        print(user)
        return token
