import pytest

from app.internal.bot.handlers import move_money_to_card, move_money_to_user
from app.internal.bot.messages import MoveMoneyToCardMessages, MoveMoneyToUserMessages
from app.internal.clean_rest.bank_account.db.models import BankAccount
from app.internal.clean_rest.cards.db.models import Card
from app.internal.clean_rest.person.db.models import Person


@pytest.mark.integration
class TestMoveMoneyToCard:
    @pytest.mark.django_db
    def test_move_money_no_args(
        self, test_update, test_context, test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card)
    ):
        user, account, card1, card2 = test_user_with_account_and_two_cards
        test_update.effective_user.id = user.telegram_id
        move_money_to_card(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(MoveMoneyToCardMessages.wrong_arguments())

    @pytest.mark.django_db
    def test_move_money(
        self, test_update, test_context, test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card)
    ):
        user, account, card1, card2 = test_user_with_account_and_two_cards
        test_update.effective_user.id = user.telegram_id
        test_context.args = [card1.number, 200, card2.number]
        move_money_to_card(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(MoveMoneyToCardMessages.success())

    @pytest.mark.django_db
    def test_move_money_invalid_amount(
        self, test_update, test_context, test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card)
    ):
        user, account, card1, card2 = test_user_with_account_and_two_cards
        test_update.effective_user.id = user.telegram_id
        test_context.args = [card1.number, "aboba", card2.number]
        move_money_to_card(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(MoveMoneyToCardMessages.invalid_amount())

    @pytest.mark.django_db
    def test_move_money_negative_amount(
        self, test_update, test_context, test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card)
    ):
        user, account, card1, card2 = test_user_with_account_and_two_cards
        test_update.effective_user.id = user.telegram_id
        test_context.args = [card1.number, -250, card2.number]
        move_money_to_card(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(MoveMoneyToCardMessages.not_positive_amount())


@pytest.mark.integration
class TestMoveMoneyToUser:
    @pytest.mark.django_db
    def test_move_money_no_args(
        self, test_update, test_context, test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card)
    ):
        user, account, card1, card2 = test_user_with_account_and_two_cards
        test_update.effective_user.id = user.telegram_id
        move_money_to_user(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(MoveMoneyToUserMessages.wrong_arguments())

    @pytest.mark.django_db
    def test_move_money_to_user(
        self,
        test_update,
        test_context,
        test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card),
        test_user_with_account_and_card: (Person, BankAccount, Card),
    ):
        user, account, card1, card2 = test_user_with_account_and_two_cards
        another_user, another_account, another_card = test_user_with_account_and_card
        test_update.effective_user.id = user.telegram_id
        test_context.args = [card1.number, 250, another_user.username]
        move_money_to_user(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(MoveMoneyToUserMessages.success())

    @pytest.mark.django_db
    def test_move_money_to_user_invalid_amount(
        self,
        test_update,
        test_context,
        test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card),
        test_user_with_account_and_card: (Person, BankAccount, Card),
    ):
        user, account, card1, card2 = test_user_with_account_and_two_cards
        another_user, another_account, another_card = test_user_with_account_and_card
        test_update.effective_user.id = user.telegram_id
        test_context.args = [card1.number, "aboba", another_user.username]
        move_money_to_user(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(MoveMoneyToUserMessages.invalid_amount())

    @pytest.mark.django_db
    def test_move_money_to_user_negative_amount(
        self,
        test_update,
        test_context,
        test_user_with_account_and_two_cards: (Person, BankAccount, Card, Card),
        test_user_with_account_and_card: (Person, BankAccount, Card),
    ):
        user, account, card1, card2 = test_user_with_account_and_two_cards
        another_user, another_account, another_card = test_user_with_account_and_card
        test_update.effective_user.id = user.telegram_id
        test_context.args = [card1.number, -250, another_user.username]
        move_money_to_user(test_update, test_context)
        test_update.message.reply_text.assert_called_once_with(MoveMoneyToUserMessages.not_positive_amount())
